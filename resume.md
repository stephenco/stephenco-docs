# Stephen Terhune

stephen@stephenco.info | 425-344-1552 | Greater Seattle Area

---

- Full-stack developer with specialty in presentation, application, and service integration layers.
- Particular expertise in building web-oriented front-end frameworks.
- Proven diverse history of delivering quality work with ongoing relationships between opportunities.
- Mission-driven forward-thinking agile team facilitator with solid results and strong performance.
- Creative, friendly, passionate, and dependable.

---


## Senior Full Stack Web Applications Software Development Engineer
9/2018 - present @ Xcarab Inc in Kirkland (contract)

> Development lead for Innate.com-related projects. Built tools and applications using mostly C# (.NET Core and some Entity Framework Core) and Javascript (including frameworks such as VueJS, AngularJS, and AngularJS, among others) and often utilizing NodeJS and Webpack for dev-ops and Azure DevOps for Git-based source control, deployment, and project management and communication.
> Windows, C#, JavaScript

## Software Development Engineer
3/2017 - 9/2018 @ Microsoft XBox GPG Accelerate Engineering in Redmond (contract)

> Built and maintained service-oriented AngularJS applications. Created advanced modular search and filtering framework providing declarative UI composition and pluggable query serialization for AngularJS applications relying on C# MVC and Dynamics365 back-ends using FetchXML. Created a front-end framework for presenting dynamic form fields inline with arbitrary text content for collecting legal agreement details from Xbox publishers. Inherited a brownfield Angular 2 Typescript application and facilitated a soft team landing while adding and fixing features as needed. Led source repository migration efforts from VSTC to Git. Primary team resource for all things Javascript, AngularJS, Typescript, Angular 2, and Git. Created, promoted, and implemented plans for script layer testing, and authored various reference documents.
> Windows, C#, Javascript

## Senior Android Software Development Engineer
10/2015 - 6/2016 @ Compound Photonics in Redmond (full-time)

> Developed software solutions for consumer- and office-based laser projector products. Created Android-based applications, designed and implemented service and application layers, integrated with third-party services, and delivered interactive user-facing surfaces for both touch and leanback experiences according to intricately designed non-Android-standard look and feel requirements. Integrated voice interaction features, motion and face recognition features (by connecting services), audio visualization features, and responded rapidly to changing requirements while continuing to deliver quality code on strict deadlines.
> Android, Java

## Senior Web Applications Software Development Engineer
8/2014 - 10/2015 @ Midfin Systems Inc in Bellevue (full-time)

> Built Ruby micro-services and API-driven tooling, developed web UIs, created command-line tools (such as plugins for Vagrant), and implemented features for the control plane layers in the Midfin SDX product family. (SDX products provide datacenter solutions for private cloud computing and infrastructure.) Leveraged various Ruby frameworks, such Sinatra, Padrino, Grape, ActiveRecord, MongoMapper, AASM, and RubyCAS Server, to produce domain models exposed over a series of REST services backed by MySQL and MongoDB. Utilized Redis and Resque for deferred processing and distribution, and a range of front-end technologies to produce single-page web applications consuming those services. Relied on build and packaging tools such as Gulp, Bower, Buildbot as well as Git for source code management. Occasionally produced graphics assets, including the the design of the company logo.
> Linux, Ruby, Javascript

## Senior Full Stack Web Applications Software Development Engineer
11/2013 - 5/2014 @ Xcarab Inc in Kirkland (contract)

> Delivered C# and Javascript code and leveraged ASP.NET MVC, MS Web API, Unity, NHibernate, Lucene.NET, Knockout.js, JQuery, and Moment.js to produce features for a large multi-tenant distributed web application for online event registration, and series of services, all strongly relying on clear command query control separation (CQRS) architecture and event sourcing. Also responsible for projects utilizing Durandal, MongoDB, and Redis for single page applications, storage, and caching.
> Windows, C#, Javascript

## Senior Full Stack Web Applications and Platform Software Development Engineer
2011 - 11/2013 @ Hipcricket in Bellevue (full-time)

> Created frameworks providing rapid prototyping and feature development for long term strategic technology improvements as well as meaningful improvements to existing and legacy codebases, mostly in the context of ASP, ASP.Net and ASP.Net MVC. Responsible for most of the work and framework-level architectural decisions related to the Hipcricket Galaxy platform to extend and unify much of the Hipcricket client-facing and internal administrative web interfaces.
> Windows, C#, Javascript

## WPF Software and Platform Development Engineer, Graphics Designer
7/2010 - 7/2011 @ Microsoft Extreme Computing Group (XCG) in Redmond (contract)

> Created framework design and developed core platform code for an event-based multi-agent platform for building large-scale distributed systems (Marlowe). Designed and built presentational applications in WPF to demonstrate various XCG projects at Microsoft TechFest. Provided clear concept representation and connectivity to various data sources, real and simulated. Delivered various custom WPF controls, created illustrations for on-screen graphics, posters, slide decks, technical papers, and a book, and influenced architectural decision-making.
> Windows, C#, WPF

## Web Applications Software Development Engineer
4/2010 - 7/2020 @ Regence Group in Seattle (contract)

> Designed and produced a library of reusable widgets and client script code for use in Regence web sites for sales and profile configuration. Lead in refactoring Salesforce markup and middle tier controller code to support front end refactoring.
> Windows, Salesforce, Java, Javascript

## Web Applications Software Development Engineer
3/2009 - 3/2010 @ Microsoft Hohm in Redmond (contract)

> Developed code for the presentation layers of the Microsoft Hohm web site (microsoft-hohm.com) and responsible for many user-facing and structural aspects of the presentation layers of the product. Practiced and promoted techniques such as functional programming, separation of concerns, progressive enhancement, various approaches to presentation markup composition, and Ajax/REST services. Team resource for JavaScript, jQuery, JSON, CSS, LINQ, ASP.NET MVC, and C# language features.
> Windows, C#, Javascript

## Senior Full Stack Software Development Engineer
2001 - 2009 @ Xcarab Inc in Kirkland (part-time while at Entirenet)

> Leveraged many languages, technologies, and techniques to provide specialized solutions for various web site and application projects, daily. Worked on both team-oriented and solo projects including an early social networking site. Participated in making design and architectural decisions for products such as the Xcarab OneXcale platform, and was often responsible for demonstrating new ideas and techniques to management, and to other developers. Met high code quality standards, estimated completion times, and then delivered solid work within rigid deadlines.
> Windows, C#, Javascript

## Software Development Engineer, Graphics Designer, Knowledge Base Writer
2002 - 2007 @ Entirenet LLC in Bellevue (full-time)

> Primarily responsible for creating knowledge base articles for MSDN. Also aided in writing and developing web sites for MS Education, MS Government, MSN TV, and other divisions, and worked as a developer on the MS internal software and utilities for KB, such as WebAce, and for MS Software Assurance, MS Office, MS TechEd, MS Mobility, and others. Produced graphical assets for Entirenet corporate web sites, the Entirenet Verlay project, and Granite Pillar (Entirenet subsidiary).
> Windows, C#, Javascript

## Software Development Engineer, Graphics Designer
1999 - 2001 @ Integrated Healthcare Partners (IHP) in Edmonds (full-time)

> Responsible for the presentation layers of an enterprise COM-based intranet platform enabling doctors and medical staff to create and share patient information (PsyOpSys). Also designed and built IHP corporate web sites, technical documentation, and various graphical assets.
> Windows, C++, Javascript

---

## Education

- Self-taught through significant research and experience, and intense on-the-job training.
- Some college electives for C++ in the middle 90's. No college degree.
- Freelance web and applications developer from 1994 ~ 2010.

---

## References

References are available by personal request.

---


Stephen Terhune | stephen@stephenco.info | 425-344-1552
